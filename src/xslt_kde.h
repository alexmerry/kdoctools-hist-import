#ifndef _MEIN_XSLT_KDE_H_
#define _MEIN_XSLT_KDE_H_

#include <QtCore/QString>

bool saveToCache(const QString &contents, const QString &filename);

#endif
