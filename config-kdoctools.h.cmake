#define XMLLINT "${LIBXML2_XMLLINT_EXECUTABLE}"

#define DOCBOOK_XML_CURRDTD "@DOCBOOKXML_CURRENTDTD_DIR@"

/* Define to 1 if you have stdio.h */
#cmakedefine01 HAVE_STDIO_H 
#cmakedefine01 HAVE_STDLIB_H
#cmakedefine01 HAVE_SYS_TYPES_H
#cmakedefine01 HAVE_SYS_STAT_H 
