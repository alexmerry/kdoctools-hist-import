# KDocTools

Create documentation from DocBook

## Introduction

Provides tools to generate documentation in various format from DocBook files.

## Authors

- Eric Bischoff   (everything not named below ;-)
- Claudiu Costin  (conversion script for Romanian and Russian TeX output)
- Frederik Fouvry (DSSSL style file customisation and SGML setup)
- David Rugge     (DocBook crash course and documentation template)

plus a number of people that made suggestions, improved scripts, sent
bug reports, etc, etc.

## Links

- Home page: <https://projects.kde.org/projects/frameworks/kdoctools>
- Mailing list: <https://mail.kde.org/mailman/listinfo/kde-docbook>
- IRC channel: #kde-devel on Freenode
- Git repository: <https://projects.kde.org/projects/frameworks/kdoctools/repository>
